def merge_sort(array1, array2)
  res = []
  index1 = 0
  index2 = 0
  while index1 < array1.length && index2 < array2.length
    if array1[index1] <= array2[index2]
      res << array1[index1]
      index1 += 1
    else
      res.push(array2[index2])
      index2 += 1
    end
  end

  while index1 < array1.length
    res.push(array1[index1])
    index1 += 1
  end

  while index2 < array2.length
    res.push(array2[index2])
    index2 += 1
  end
  res
end

def recursive_merge_sort(array)
  return array if array.length < 2

  half_length = (array.length / 2).floor
  other_half_length = array.length - half_length

  merge_sort(recursive_merge_sort(array.slice(0, half_length)),
             recursive_merge_sort(array.slice(half_length, other_half_length)))
end

def full_merge_sort(array)
  # Convert array to hash
  hash = {}
  array.each do |value|
    integer = value.split(' ')[0]
    string = value.split(' ')[1]
    integer = integer.to_i
    hash[integer] = [] unless hash[integer]
    hash[integer].push([string])
  end

  array_of_keys = hash.keys
  sorted_keys = recursive_merge_sort(array_of_keys)

  final_string_array = []

  sorted_keys.each do |value|
    final_string_array.push(hash[value])
  end

  final_string_array.flatten
end

full_merge_sort(['0 ab', '6 cd', '0 ef', '6 gh', '4 ij', '0 ab', '6 cd', '0 ef', '6 gh', '0 ij', '4 that', '3 be',
                 '0 to', '1 be', '5 question', '1 or', '2 not', '4 is', '2 to', '4 the'])
# => ["ab", "ef", "ab", "ef", "ij", "to", "be", "or", "not", "to", "be", "ij", "that", "is", "the", "question", "cd", "gh", "cd", "gh"]

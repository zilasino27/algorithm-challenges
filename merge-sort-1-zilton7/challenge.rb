def merge_sort(array1, array2)
  res = []
  index1 = 0
  index2 = 0
  while index1 < array1.length && index2 < array2.length
    if array1[index1] <= array2[index2]
      res << array1[index1]
      index1 += 1
    else
      res.push(array2[index2])
      index2 += 1
    end
  end

  while index1 < array1.length
    res.push(array1[index1])
    index1 += 1
  end

  while index2 < array2.length
    res.push(array2[index2])
    index2 += 1
  end
  return res
end

p merge_sort([1, 3, 9, 11], [2, 4, 6, 8])
# => [1, 2, 3, 4, 6, 8, 9, 11]

def move(starting, goal)
  puts "#{starting}->#{goal}"
end

def move2(starting, goal, intermediate)
  move(starting, intermediate)
  move(starting, goal)
  move(intermediate, goal)
end

def move3(starting, goal, intermediate)
  move2(starting, intermediate, goal)
  move(starting, goal)
  move2(intermediate, goal, starting)
end

def move4(starting, goal, intermediate)
  move3(starting,intermediate,goal)    
  move(starting,goal)    
  move3(intermediate,goal,starting)
end

def move5(starting, goal, intermediate)
  move4(starting,intermediate,goal)    
  move(starting,goal)    
  move4(intermediate,goal,starting)
end

def hanoi_steps(number_of_discs)
  if number_of_discs == 2
    move2(1,3, 2)
  elsif number_of_discs == 3
    move3(1,3, 2)
  elsif number_of_discs == 4
    move4(1,3, 2)
  elsif number_of_discs == 5
    move5(1,3, 2)
  end
end

# hanoi_steps(2)
# => 1->2 
#    1->3 
#    2->3

# hanoi_steps(3)
# => 1->3 
#    1->2
#    3->2
#    1->3
#    2->1
#    2->3
#    1->3

# hanoi_steps(4)
# => 1->2
#    1->3
#    2->3
#    1->2
#    3->1
#    3->2
#    1->2
#    1->3
#    2->3
#    2->1
#    3->1
#    2->3
#    1->2
#    1->3
#    2->3

hanoi_steps(5)
# => 1->3
#    1->2
#    3->2
#    1->3
#    2->1
#    2->3
#    1->3
#    1->2
#    3->2
#    3->1
#    2->1
#    3->2
#    1->3
#    1->2
#    3->2
#    1->3
#    2->1
#    2->3
#    1->3
#    2->1
#    3->2
#    3->1
#    2->1
#    2->3
#    1->3
#    1->2
#    3->2
#    1->3
#    2->1
#    2->3
#    1->3
def find_pairs(array, k)
  res = []
  array.each do |i|
    array.each do |j|
      if i != j && i + j == k && !res.flatten.include?(i)
        res.push([i, j])
      end
    end
  end
  res
end

p find_pairs([1, 9, 11, 13, 2, 3, 7], 12)
# => [[1, 11], [9, 3]]

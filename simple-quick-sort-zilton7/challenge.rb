def partition(array, original)
  return array.join("") if array.size <= 1

  pivot = array.first
  left = []
  right = []
  (1...array.size).each do |i|
    if array[i] < pivot
      left.push(array[i])
    elsif array[i] > pivot
      right.push(array[i])
    end
  end

  if left.size == 1 && right.empty?
    puts("#{left.join("")} #{[pivot].join(" ")}")
    return "#{left.join("")} #{[pivot].join(" ")}" 
  end

  if right.size == 1 && left.empty?
    puts ("#{pivot} #{right.join(" ")}")
    return "#{pivot} #{right.join(" ")}" 
  end

  var = ("#{partition(left, false)} #{pivot} #{partition(right, false)}").strip
  if original == false
    puts var
    return var
  end
  
  var
end

def simple_quicksort(array)
 puts partition(array, true)
end

# 1 2
# 4 5
# 1 2 3 4 5
# 1 2 3 4 5 6 7
# 1 2 3 4 5 6 7 8
# 1 2 3 4 5 6 7 8 9
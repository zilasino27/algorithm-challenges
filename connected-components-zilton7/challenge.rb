def bfs(graph)
  result = [0]
  q = [0]
  while !q.empty?
    key = q.shift
    graph[key].each do |i|
      unless result.include?(i)
        result.push(i)
        q.push(i)
      end
    end
  end
  result
end

def connected_graph?(graph)
    bfs(graph).sort == graph.keys
end

p connected_graph?({
  0 => [2], 
  1 => [4], 
  2 => [0, 5, 3], 
  3 => [5, 2], 
  4 => [5, 1], 
  5 => [4, 2, 3]
})
# => true

p connected_graph?({
  0 => [1, 2], 
  1 => [0, 2], 
  2 => [0, 1, 3, 4, 5], 
  3 => [2, 4], 
  4 => [3, 2], 
  5 => [2]
})
# => true

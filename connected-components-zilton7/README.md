## How to solve this challenge?

1. Read the "Challenge description" below.
2. Make changes to the [challenge.rb](./challenge.rb) file.
3. Commit your changes.
4. Wait for the result of the "GitHub Classroom Workflow" action. If it is green - congratulations, you solved this challenge! If not - try again!
5. *You can watch an example of how to solve a challenge in [this video](https://microverse.pathwright.com/library/fast-track-algorithms-data-structures/69123/path/step/113963868/)*

Note: We use RSpec for checking your solution with unit tests. You can [install](https://github.com/rspec/rspec) it and use it in your local environment if you like.


## Challenge description

### Connected Components
_Are all the components connected on the given Graph?_

A graph is considered one connected component if every node is connected along a path with every other node.

The following graph is not one connected component, since the 1 and 4 cannot be reached from the 0,2,3 and 5.

![](graph.png)

### Challenge
Given a graph, return true if the graph is one connected component, and false otherwise.
By using a search algorithm (BFS/DFS), you can traverse through the entire graph and save visited nodes. If you visited all nodes listed in the original graph, it means that all of them are connected.

### Example
```ruby
graph = {
  0 => [2], 
  1 => [4], 
  2 => [0, 5, 3], 
  3 => [5, 2], 
  4 => [5, 1], 
  5 => [4, 2, 3]
}

puts connected_graph?(graph)
# => true
```

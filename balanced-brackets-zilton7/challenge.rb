def balanced_brackets?(string)
  stack = []
  opening = ['(', '[', '{']
  closing = [')', ']', '}']

  splitted = string.split('')

  splitted.each do |el| 
    if opening.include? el
      stack.push(el)
    elsif closing.include? el
      return false if stack.empty?  # if there is closing el, but stack is empty it unbalanced!
      top = stack.pop
      if top == '(' && el != ')' || top == '[' && el != ']' || top == '{' && el != '}'
        return false
      end
    end
  end

  stack.empty?
end

puts balanced_brackets?('(hello)[world]')
# => true

puts balanced_brackets?('([)]')
# => false

puts balanced_brackets?('[({}{}{})([])]')
# => true

# https://www.youtube.com/watch?v=QZOLb0xHB_Q
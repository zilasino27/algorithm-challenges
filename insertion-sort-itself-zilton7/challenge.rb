def sort_itself(array)
  (1...array.length).each do |index|
    num = array[index]
    current_index = index - 1
    
    while num < array[current_index]
      break if current_index < 0 
      array[current_index + 1] = array[current_index]
      current_index = current_index - 1
    end

    array[current_index + 1] = num
    puts array.join(' ')
  end
end



sort_itself([1, 4, 3, 6, 9, 2])
# => 1 4 3 6 9 2
#    1 3 4 6 9 2
#    1 3 4 6 9 2
#    1 3 4 6 9 2
#    1 2 3 4 6 9

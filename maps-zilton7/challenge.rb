def appears_most_times(array)
  hash = {}
  array.each do |item|
    if hash[item]
      hash[item] += 1
    else
      hash[item] = 1
    end
  end
  hash.max_by{|key, value| value}.first
end

p appears_most_times([1, 2, 3, 1, 5])
# => 1

p appears_most_times([23, 43, 67, 88, 42, 35, 77, 88, 99, 11])
# => 88

p appears_most_times([4376, -345, -345, 4376, -345, 84945, 4376, -345, -26509, 4376, 84945, 84945, -26509, 896341, 4376])
# => 4376

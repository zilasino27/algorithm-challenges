class Node
  attr_accessor :value, :next_node
  
  def initialize(value, next_node = nil)
	  @value = value
    @next_node = next_node
  end
end

class LinkedList
  #setup head and tail
  def initialize
    @head = nil
  end
  
  def add(number)
    new_node = Node.new(number)
    if @head.nil?
      @head = new_node
    elsif @head.next_node.nil?
      @head.next_node = new_node
      @last = new_node
    else
      @last.next_node = new_node
      @last = new_node
    end   
  end

  def get(index)
    node = @head
    current = node
    iter = 0
    while iter < index
      current = node.next_node
      node = current
      iter+=1
    end
    current.value
  end
end

list = LinkedList.new

list.add(3)
list.add(5)
list.add(7)
# p list
puts list.get(0)
# => 5
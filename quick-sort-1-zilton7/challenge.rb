def partition(array)
  left = []
  right = []
  pivot = array.delete_at(0)
  array.each do |n|
    if n <= pivot
      left.push(n)
    else
      right.push(n)
    end
  end
  (left + [pivot] + right)
end

p partition([4, 5, 3, 9, 1])
# => [3, 1, 4, 5, 9]

def running_time(array)
  shifts = 0
  (1...array.length).each do |index|
    num = array[index]
    current_index = index - 1
    
    while num < array[current_index]
      array_before_shift = [*array]
      break if current_index < 0 
      array[current_index + 1] = array[current_index]
      current_index = current_index - 1
      if array_before_shift != array
        shifts+=1
      end
    end

    array[current_index + 1] = num
  end

  return shifts

end

puts running_time([2, 1, 3, 1, 2])
# => 4
